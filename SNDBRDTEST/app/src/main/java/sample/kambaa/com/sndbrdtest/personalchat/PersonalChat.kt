package sample.kambaa.com.sndbrdtest.personalchat

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_personal_chat.*
import sample.kambaa.com.sndbrdtest.R

class PersonalChat : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal_chat)
        try{
            supportFragmentManager.beginTransaction().replace(R.id.body_container, PersonalListFragment()).commit()
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    public  fun setToolbartitle(title:String){
        try{
            personal_chat_toolbar_title?.setText(title)
            personal_chat_back?.setOnClickListener {
                onBackPressed()
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
