package sample.kambaa.com.sndbrdtest.groupchannel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import sample.kambaa.com.sndbrdtest.R;
import sample.kambaa.com.sndbrdtest.main.SettingsActivity;


public class GroupChannelActivity extends AppCompatActivity {


    private boolean doubleBackToExitPressedOnce=false;
    private ImageView backIcon,menuimg;
    private TextView title,lastseenoract;
    private FragmentActivity activity =this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_channel);
        getSupportActionBar().hide();
        if (savedInstanceState == null) {
            // Load list of Group Channels
            Fragment fragment = GroupChannelListFragment.newInstance();

            FragmentManager manager = getSupportFragmentManager();
            manager.popBackStack();
            manager.beginTransaction()
                    .replace(R.id.container_group_channel, fragment)
                    .addToBackStack(null)
                    .commit();
        }

        String channelUrl = getIntent().getStringExtra("groupChannelUrl");
        if(channelUrl != null) {
            // If started from notification
            Fragment fragment = GroupChatFragment.newInstance(channelUrl);
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.container_group_channel, fragment)
                    .addToBackStack(null)
                    .commit();
        }

        backIcon =findViewById(R.id.back_chats);
        title =findViewById(R.id.chat_title);
        lastseenoract =findViewById(R.id.chat_lastseen);
        menuimg =findViewById(R.id.menu_settings);

    }

    interface onBackPressedListener {
        boolean onBack();
    }
    private onBackPressedListener mOnBackPressedListener;

    public void setOnBackPressedListener(onBackPressedListener listener) {
        mOnBackPressedListener = listener;
    }

    @Override
    public void onBackPressed() {
        if (mOnBackPressedListener != null && mOnBackPressedListener.onBack()) {
            return;
        }

        Fragment fragmentManager = getSupportFragmentManager().findFragmentById(R.id.container_group_channel);
        if(fragmentManager instanceof GroupChannelListFragment ){
            checkbackpress();
        }else if(fragmentManager instanceof  GroupChatFragment){
            Fragment fragment = new GroupChannelListFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.container_group_channel, fragment)
                    .addToBackStack(null)
                    .commit();
        }
        else{
            super.onBackPressed();
        }
    }

    private void checkbackpress() {
        try {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                finish();
                return;
            }

            doubleBackToExitPressedOnce = true;

            Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void setActionBarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    public void  settitle(String titile){
        try{
            lastseenoract.setVisibility(View.GONE);
            menuimg.setVisibility(View.GONE);
            title.setText(titile);
            backIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    public void  setchatsList(String titile){
        try{
            menuimg.setVisibility(View.VISIBLE);
            title.setText(titile);
            lastseenoract.setVisibility(View.GONE);
            backIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            menuimg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(activity,SettingsActivity.class));
                }
            });
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void  settitlelastseen(String titile,String lastseen){
        Log.d("LASTSEEN"," 22 "+lastseen);
        try{
            lastseenoract.setVisibility(View.VISIBLE);
            menuimg.setVisibility(View.GONE);
            title.setText(titile);
            lastseenoract.setText(lastseen);
            backIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
