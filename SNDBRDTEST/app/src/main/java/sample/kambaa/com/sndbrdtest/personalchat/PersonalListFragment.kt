package sample.kambaa.com.sndbrdtest.personalchat


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_personal_list.*

import sample.kambaa.com.sndbrdtest.R

class PersonalListFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        (activity as PersonalChat).setToolbartitle(getString(R.string.chats))
        return inflater.inflate(R.layout.fragment_personal_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
            try{
                swipe_personal_chat_list?.setOnRefreshListener {
                    try{
                        refresh()
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }
            }catch (ex:Exception){
                ex.printStackTrace()
            }
    }

    private fun refresh() {
        try{


        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }


}
