package sample.kambaa.com.sndbrdtest.main;


import android.app.Application;
import android.support.v4.app.FragmentActivity;
import com.sendbird.android.SendBird;
import sample.kambaa.com.sndbrdtest.R;
import sample.kambaa.com.sndbrdtest.utils.PreferenceUtils;

public class BaseApplication extends Application {

    private static final String APP_ID = "2BD7BE55-AEFE-46A3-81EA-FF186CBE291F"; // US-1 Demo
    public static final String VERSION = "3.0.40";

    @Override
    public void onCreate() {
        super.onCreate();
        PreferenceUtils.init(getBaseContext());

//        SendBird.init(getString(R.string.send_bird_app_id), getApplicationContext());
        SendBird.init(APP_ID, getApplicationContext());
    }
}
