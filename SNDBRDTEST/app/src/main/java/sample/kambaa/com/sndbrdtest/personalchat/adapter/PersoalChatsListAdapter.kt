package sample.kambaa.com.sndbrdtest.personalchat.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sendbird.android.OpenChannel
import sample.kambaa.com.sndbrdtest.R
import sample.kambaa.com.sndbrdtest.main.interfaces.OnclickEvent

class PersoalChatsListAdapter(val context: Context,val chatuserList:ArrayList<OpenChannel>,val onclickEvent: OnclickEvent)
    :RecyclerView.Adapter<PersoalChatsListAdapter.Viewholder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PersoalChatsListAdapter.Viewholder {
        return Viewholder(LayoutInflater.from(context).inflate(R.layout.list_item_open_channel,p0,false))
    }

    override fun getItemCount(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBindViewHolder(p0: PersoalChatsListAdapter.Viewholder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

}