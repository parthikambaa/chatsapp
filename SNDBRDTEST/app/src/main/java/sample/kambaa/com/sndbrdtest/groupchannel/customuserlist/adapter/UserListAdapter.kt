package sample.kambaa.com.sndbrdtest.groupchannel.customuserlist.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.sendbird.android.User
import sample.kambaa.com.sndbrdtest.R
import sample.kambaa.com.sndbrdtest.main.interfaces.OnclickEvent
import sample.kambaa.com.sndbrdtest.utils.ImageUtils
@SuppressLint("SetTextI18n")
class UserListAdapter(val context: Context,val userArrayList: ArrayList<User>,val onclickEvent: OnclickEvent)
    :RecyclerView.Adapter<UserListAdapter.Viewholder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): UserListAdapter.Viewholder {
        return Viewholder(LayoutInflater.from(context).inflate(R.layout.adpater_user_list,p0,false))
    }

    override fun getItemCount(): Int {   return userArrayList.size   }


    override fun onBindViewHolder(p0: UserListAdapter.Viewholder, p1: Int) {
        try {
            Log.d("imageUrl",""+userArrayList[p1].originalProfileUrl)
            if(userArrayList[p1].originalProfileUrl!=null && !userArrayList[p1].originalProfileUrl.isEmpty()){
                ImageUtils.displayRoundImageFromUrl(context,userArrayList[p1].originalProfileUrl,p0.usrimage)
            }else{
                p0.usrimage.setImageResource(R.drawable.add_user)
            }
            p0.usrname.text = userArrayList[p1].nickname.substring(0,1).toUpperCase()+userArrayList[p1].nickname.substring(1)
            p0.constraintLayout.setOnClickListener {
                onclickEvent.clickPosition(p1.toString(),userArrayList.size.toString())
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }


    class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val usrimage = itemView.findViewById<ImageView>(R.id.adp_userimageview)!!
        val usrname = itemView.findViewById<TextView>(R.id.adp_user_name)!!
        val constraintLayout = itemView.findViewById<ConstraintLayout>(R.id.adp_linear_layout)!!
    }
}