package sample.kambaa.com.sndbrdtest.groupchannel.customuserlist

import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.sendbird.android.*
import kotlinx.android.synthetic.main.activity_user_list.*
import sample.kambaa.com.sndbrdtest.R
import sample.kambaa.com.sndbrdtest.groupchannel.CreateGroupChannelActivity
import sample.kambaa.com.sndbrdtest.groupchannel.GroupChannelActivity
import sample.kambaa.com.sndbrdtest.groupchannel.GroupChannelListFragment.INTENT_REQUEST_NEW_GROUP_CHANNEL
import sample.kambaa.com.sndbrdtest.groupchannel.GroupChatFragment
import sample.kambaa.com.sndbrdtest.groupchannel.customuserlist.adapter.UserListAdapter
import sample.kambaa.com.sndbrdtest.main.LoginActivity.CURRENT_USER
import sample.kambaa.com.sndbrdtest.main.interfaces.OnclickEvent
import sample.kambaa.com.sndbrdtest.utils.MessageUtils
import sample.kambaa.com.sndbrdtest.utils.PreferenceUtils

@Suppress("UNREACHABLE_CODE")
class UserListActivity : Fragment() ,OnclickEvent{


       var  userList=ArrayList<User>()
       var mUserListQuery: ApplicationUserListQuery? = null
       var userListAdapter :UserListAdapter?=null
        var TAG ="UserList"
        var username=""
        var userimage=""
    var dialog:Dialog?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as GroupChannelActivity).settitle(getString(R.string.select_user))
        return inflater.inflate(R.layout.activity_user_list,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
            dialog = Dialog(activity)
        /**
         * swiperefreshLayout
         */
        swipe_user_list?.setOnRefreshListener {
            try{
                getUserList()
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }

        /**
         * addgroup
         */
        linearaddgroup?.setOnClickListener {
            try{
                startActivityForResult(Intent(activity,CreateGroupChannelActivity::class.java),INTENT_REQUEST_NEW_GROUP_CHANNEL)
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }
        getUserList()
    }

    private fun getUserList() {
        dialog =MessageUtils.showDialog(activity)
        try{
             mUserListQuery=SendBird.createApplicationUserListQuery()
            mUserListQuery?.next { p0, p1 ->
                if(p1!=null){
                    return@next
                }else{
                    userList = p0 as ArrayList<User>
                    setToADpter()
                }
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }



    private fun setToADpter() {
        if(dialog!=null&& dialog?.isShowing!!){
            MessageUtils.dismissDialog(dialog)
        }
        swipe_user_list?.isRefreshing =false
//        userList.clear()//Testing purpose
        try{
            if(userList.size!=0){
                /*Log.d(TAG,""+ CURRENT_USER.nickname)
                Log.d(TAG,""+ CURRENT_USER.userId)
                Log.d(TAG,""+ userList.size)*/
                var pos=0
                for((i, user) in userList.withIndex()){
                    Log.d(TAG,""+user.userId)
                    if(PreferenceUtils.getUserId().equals(user.userId)){
                        Log.d(TAG,""+user.nickname+"Equals")
                        pos=i
                    }
                }
                userList.removeAt(pos)
                Log.d(TAG,""+ userList.size)
                tv_empty_usr_list?.visibility =View.GONE
                recyler_userList?.visibility =View.VISIBLE
                recyler_userList?.layoutManager = LinearLayoutManager(activity,LinearLayout.VERTICAL,false) as RecyclerView.LayoutManager?
                userListAdapter =UserListAdapter(activity!!,userList,this)
                recyler_userList?.adapter  =userListAdapter
                linearaddgroup?.visibility =View.VISIBLE
            }else{
                linearaddgroup?.visibility =View.GONE
                recyler_userList?.visibility =View.GONE
                tv_empty_usr_list?.visibility =View.VISIBLE
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
    override fun clickPosition(position: String, name: String) {
        try{
            dialog =MessageUtils.showDialog(activity)
            val selected=ArrayList<User>()
            username = userList[position.toInt()].nickname
            userimage = userList[position.toInt()].originalProfileUrl
            selected.add(userList[position.toInt()])
            selected.add(CURRENT_USER)
            createchennals(selected, PreferenceUtils.getGroupChannelDistinct())
            Log.d(TAG,position+""+name)
        }catch (ex:Exception){ex.printStackTrace()}
    }

    private fun createchennals(selected: ArrayList<User>, b: Boolean) {
        try{
            /**
             * create only two users chats
             */
            GroupChannel.createChannel(selected, b, username, userimage, null, null, GroupChannel.GroupChannelCreateHandler{
                    groupChannel, e ->
                if (e != null) {
                    // Error.
                    Log.d(TAG, "" + e)
                    return@GroupChannelCreateHandler
                    MessageUtils.dismissDialog(dialog)
                } else {
                    MessageUtils.dismissDialog(dialog)
                    val fragment = GroupChatFragment.newInstance(groupChannel.url)
                    val manager = fragmentManager
                    manager!!.beginTransaction()
                        .replace(R.id.container_group_channel, fragment)
                        .addToBackStack(null)
                        .commit()
                }
            })

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("GrChLIST", "" + requestCode)
        if (requestCode == INTENT_REQUEST_NEW_GROUP_CHANNEL) {
            Log.d("GrChLIST", "" + requestCode)
            if (resultCode == RESULT_OK) {
                // Channel successfully created
                // Enter the newly created channel.
                val newChannelUrl = data?.getStringExtra(CreateGroupChannelActivity.EXTRA_NEW_CHANNEL_URL)!!
                if (newChannelUrl != null) {
                    enterGroupChannel(newChannelUrl)
                }
            } else {
                Log.d("GrChLIST", "resultCode not STATUS_OK")
            }
        }

    }

    internal fun enterGroupChannel(channelUrl: String) {
        val fragment = GroupChatFragment.newInstance(channelUrl)
        fragmentManager!!.beginTransaction()
            .replace(R.id.container_group_channel, fragment)
            .addToBackStack(null)
            .commit()
    }

}
