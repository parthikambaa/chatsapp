package sample.kambaa.com.sndbrdtest.groupchannel;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.GroupChannelParams;
import com.sendbird.android.SendBirdException;
import sample.kambaa.com.sndbrdtest.R;
import sample.kambaa.com.sndbrdtest.utils.ImageUtils;
import sample.kambaa.com.sndbrdtest.utils.MessageUtils;
import sample.kambaa.com.sndbrdtest.utils.PathUtil;
import sample.kambaa.com.sndbrdtest.utils.PreferenceUtils;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * An Activity to create a new Group Channel.
 * First displays a selectable list of users,
 * then shows an option to create a Distinct channel.
 */

public class CreateGroupChannelActivity extends AppCompatActivity
        implements SelectUserFragment.UsersSelectedListener, SelectDistinctFragment.DistinctSelectedListener {

    public static final String EXTRA_NEW_CHANNEL_URL = "EXTRA_NEW_CHANNEL_URL";

    static final int STATE_SELECT_USERS = 0;
    static final int STATE_SELECT_DISTINCT = 1;
    View groupDialogView;
    AlertDialog dialog;
    private static final int PERMISSION_REQUEST_CODE = 1;

    private Button mNextButton;

    private ImageView mNextimg;
    ImageView imgGroupImage;
    AlertDialog.Builder builder;
    Uri imageUri=null;

    private List<String> mSelectedIds;
    private boolean mIsDistinct = true;

    private int mCurrentState;
    private FloatingActionButton mAddnewContact;

    private Toolbar mToolbar;
    private String TAG="CreateGroup";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_group_channel);
        getSupportActionBar().hide();
        mSelectedIds = new ArrayList<>();

        if (savedInstanceState == null) {
            Fragment fragment = SelectUserFragment.newInstance();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.container_create_group_channel, fragment)
                    .commit();
        }

        mNextButton = (Button) findViewById(R.id.button_create_group_channel_next);
//        mAddnewContact =findViewById(R.id.add_new_contact_group_list);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentState == STATE_SELECT_USERS) {
                    Fragment fragment = SelectDistinctFragment.newInstance();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container_create_group_channel, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            }
        });
        mNextButton.setEnabled(false);

        mNextimg = findViewById(R.id.button_create_group_channel_create);
        mNextimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentState == STATE_SELECT_USERS) {
//                if (mCurrentState == STATE_SELECT_DISTINCT) {
                    mIsDistinct = PreferenceUtils.getGroupChannelDistinct();
                    geticonandname(mSelectedIds,mIsDistinct);

                }
            }
        });
     /*   mAddnewContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CreateGroupChannelActivity.this,CreateOpenChannelActivity.class));
            }
        });*/
        mNextimg.setVisibility(View.GONE);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_create_group_channel);
      /*  setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24_dp);
        }*/

    }

    private void geticonandname(final List<String> mSelectedIds, final boolean mIsDistinct) {
        try{
            builder =new AlertDialog.Builder(this);
            builder.setCancelable(false);
            dialog  =builder.create();
            LayoutInflater  layoutInflater = LayoutInflater.from(this);
            groupDialogView = layoutInflater.inflate(R.layout.dialog_upload_gruop_name,null,false);
            dialog.setView(groupDialogView);
            dialog.show();
            TextView cancel,create;
            RelativeLayout imgrelativelayou;

            final EditText edtGroupName;

            imgrelativelayou = groupDialogView.findViewById(R.id.imge_relativelayout);
            imgGroupImage = groupDialogView.findViewById(R.id.image_view_profile_dialog);
            cancel = groupDialogView.findViewById(R.id.btn_cancel);
            create = groupDialogView.findViewById(R.id.btn_create);
            edtGroupName = groupDialogView.findViewById(R.id.edt_group_name);

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                            if(dialog!=null && dialog.isShowing()){
                                dialog.dismiss();
                            }
                }
            });
            imgrelativelayou.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                     requestPermission();
                    }
                });
            create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{
                        if(validate(edtGroupName.getText().toString(),imageUri)){
                            creategroupChats(mSelectedIds, mIsDistinct,edtGroupName.getText().toString(),imageUri);
                        }
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            });
//            createGroupChannel(mSelectedIds, mIsDistinct);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void creategroupChats(List<String> mSelectedIds, boolean mIsDistinct,String groupname,Uri imageUri) {
        try {
            GroupChannelParams params = new GroupChannelParams()
                    .setPublic(false)
                    .setEphemeral(false)
                    .setDistinct(mIsDistinct)
                    .addUserIds(mSelectedIds)
//                    .setOperatorIds(operatorIds)
                    .setName(groupname)
                    .setCoverImage(new File(PathUtil.getPath(this,imageUri)));    // or .setCoverUrl(COVER_URL)
                    /*.setData(DATA)
                    .setCustomType(CUSTOM_TYPE);*/

            GroupChannel.createChannel(params, new GroupChannel.GroupChannelCreateHandler() {
                @Override
                public void onResult(GroupChannel groupChannel, SendBirdException e) {
                    if (e != null) {
                        // Error.
                        Log.d(TAG,""+e);
                        return;
                    }else{
                        if(dialog!=null&&dialog.isShowing()){
                            dialog.dismiss();
                        }
                        Intent intent = new Intent();
                        intent.putExtra(EXTRA_NEW_CHANNEL_URL, groupChannel.getUrl());
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            });

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private boolean validate(String gruopname, Uri imageUri) {
        try{
            if(gruopname.isEmpty()){
                MessageUtils.showSnackBar(this,groupDialogView,"Group Name Can't be Empty");
                return  false;
            }else if (imageUri==null){
                MessageUtils.showSnackBar(this,groupDialogView,"Group Icon Can't be Empty");
                return  false;
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return  true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void setState(int state) {
        if (state == STATE_SELECT_USERS) {
            mCurrentState = STATE_SELECT_USERS;
            mNextimg.setVisibility(View.GONE);
            mNextButton.setVisibility(View.GONE);
//            mCreateButton.setVisibility(View.GONE);
//            mNextButton.setVisibility(View.VISIBLE);
        } else if (state == STATE_SELECT_DISTINCT){
            mCurrentState = STATE_SELECT_DISTINCT;
            mNextimg.setVisibility(View.GONE);
            mNextButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onUserSelected(boolean selected, String userId) {
        if (selected) {
            mSelectedIds.add(userId);
        } else {
            mSelectedIds.remove(userId);
        }

        if (mSelectedIds.size() > 0) {
            mNextimg.setVisibility(View.VISIBLE);
//            mNextButton.setEnabled(true);
        } else {
            mNextimg.setVisibility(View.GONE);
//            mNextButton.setEnabled(false);
        }
    }

    @Override
    public void onDistinctSelected(boolean distinct) {
        mIsDistinct = distinct;
    }

    /**
     * Creates a new Group Channel.
     *
     * Note that if you have not included empty channels in your GroupChannelListQuery,
     * the channel will not be shown in the user's channel list until at least one message
     * has been sent inside.
     *
     * @param userIds   The users to be members of the new channel.
     * @param distinct  Whether the channel is unique for the selected members.
     *                  If you attempt to create another Distinct channel with the same members,
     *                  the existing channel instance will be returned.
     */
    private void createGroupChannel(List<String> userIds, boolean distinct) {
        GroupChannel.createChannelWithUserIds(userIds, distinct, new GroupChannel.GroupChannelCreateHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {
                    // Error!
                    return;
                }
                Log.d(TAG,""+groupChannel.getCustomType());
                Log.d(TAG,""+groupChannel.getMemberCount());

                Intent intent = new Intent();
                intent.putExtra(EXTRA_NEW_CHANNEL_URL, groupChannel.getUrl());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    private void requestPermission() {
        if ( ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                ) {
            callintenttoread();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    ) {
//            Toast.makeText(this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
                callintenttoread();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            }
        }

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] + grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    callintenttoread();
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }
    private void callintenttoread() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 12);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,""+requestCode+""+resultCode);
        if(requestCode ==12 && resultCode==Activity.RESULT_OK && data!=null){
            Log.d(TAG,""+requestCode+""+data.getData().toString());
            try {
                imageUri = data.getData();
                ImageUtils.displayRoundImageFromUrl(this,imageUri.toString(),imgGroupImage);
            /*    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                  final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                imgGroupImage.setImageBitmap(selectedImage);*/
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else{

        }
    }



}
