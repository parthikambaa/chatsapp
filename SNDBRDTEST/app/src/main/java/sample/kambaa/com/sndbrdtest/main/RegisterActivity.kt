package sample.kambaa.com.sndbrdtest.main

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.UserDictionary.Words.APP_ID
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.sendbird.android.SendBird
import kotlinx.android.synthetic.main.activity_register.*
import sample.kambaa.com.sndbrdtest.R
import sample.kambaa.com.sndbrdtest.utils.PreferenceUtils
import sample.kambaa.com.sndbrdtest.utils.PushUtils

import com.sendbird.android.SendBird.ConnectHandler
import sample.kambaa.com.sndbrdtest.groupchannel.GroupChannelActivity
import sample.kambaa.com.sndbrdtest.openchannel.OpenChannelActivity
import sample.kambaa.com.sndbrdtest.personalchat.PersonalChat


@SuppressLint("StaticFieldLeak")
class RegisterActivity : AppCompatActivity() {

    var TAG ="REGISTER"

    companion object {

        var snackView: View? = null
    }
        var chattypeView:View?=null
        var chattDialogBuilder:AlertDialog.Builder?=null
        var chattDialog:AlertDialog?=null
        var diaPersonlChat:TextView?=null
        var diaGroupChat:TextView?=null
        var dialogout:TextView?=null
        var diaaddnewcontact:TextView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        chattDialogBuilder=AlertDialog.Builder(this)
        snackView = findViewById(R.id.actvity_register)
        val username = sndBrd_user_name?.text.toString()
        val emailid = sndBrd_email?.text.toString()
        val mobile = sndBrd_mobile_num?.text.toString()
        sndBrd_register?.setOnClickListener {
            try {
                PreferenceUtils.setUserId(mobile)
                PreferenceUtils.setUsername(username)
               val init= SendBird.init(getString(R.string.send_bird_app_id),this)
                Log.d(TAG,""+init)
//                connectTosendBird(mobile, username)
                if(PushUtils.isNetworkConnected(this)){
                    connectSendBirds()
                }else{
                    Log.d(TAG,"No Internet SErvice")
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

    }

    private fun connectSendBirds() {
        try{
            var userId =sndBrd_user_name?.text?.toString()!!
            userId = userId.replace("\\s", "")
            SendBird.connect(userId, ConnectHandler { user, e ->

                if (e != null) {    // Error.
                 Log.d(TAG,""+e.printStackTrace())
                    return@ConnectHandler
                }else {
                    callpopupDialog()
                }

            })
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    private fun callpopupDialog() {
        try{
            Log.d(TAG,"dialogfunction")
            val layoutInflater =LayoutInflater.from(this)
            chattypeView =layoutInflater.inflate(R.layout.dialog_chattype,null,false)
            chattDialogBuilder?.setCancelable(false)
            chattDialog = chattDialogBuilder?.create()
            chattDialog?.setView(chattypeView)
            chattDialog?.show()
            diaPersonlChat = chattypeView?.findViewById(R.id.dig_personal_chat)
            diaGroupChat = chattypeView?.findViewById(R.id.dig_group_chat)
            dialogout = chattypeView?.findViewById(R.id.dig_logout)
            diaaddnewcontact = chattypeView?.findViewById(R.id.dig_add_new_contact)
            diaPersonlChat?.setOnClickListener {
                if(chattDialog!=null&&chattDialog?.isShowing!!){
                    chattDialog?.dismiss()
                }
                val intent = Intent(this, OpenChannelActivity::class.java)
                startActivity(intent)
            }
            diaGroupChat?.setOnClickListener {
                if(chattDialog!=null&&chattDialog?.isShowing!!){
                    chattDialog?.dismiss()
                }
                val intent = Intent(this, GroupChannelActivity::class.java)
                startActivity(intent)
            }

            dialogout?.setOnClickListener {
                try{
                    if(chattDialog!=null&&chattDialog?.isShowing!!){
                        chattDialog?.dismiss()
                    }

                }catch (ex:Exception){
                    ex.printStackTrace()

                }
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }


    // Displays a Snackbar from the bottom of the screen
    private fun showSnackbar(text: String) {
        val snackbar = Snackbar.make(snackView!!, text, Snackbar.LENGTH_SHORT)
        snackbar.show()
    }

    private fun updateCurrentUserPushToken() {
        PushUtils.registerPushTokenForCurrentUser(this, null)
    }
    private fun updateCurrentUserInfo(userNickname: String) {
        SendBird.updateCurrentUserInfo(userNickname, null, SendBird.UserInfoUpdateHandler { e ->
            if (e != null) {
                // Error!
                Toast.makeText(
                    this, "" + e.code + ":" + e.message,
                    Toast.LENGTH_SHORT
                )
                    .show()

                // Show update failed snackbar
                showSnackbar("Update user nickname failed")

                return@UserInfoUpdateHandler
            }

            PreferenceUtils.setUsername(userNickname)
        })
    }

}
